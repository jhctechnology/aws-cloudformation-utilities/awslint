#!/bin/bash -e

if [ -e cfn-deploy/check_stack_final_status.sh ]; then
  # shellcheck disable=SC1091
  . cfn-deploy/check_stack_final_status.sh
else
  # shellcheck disable=SC1091
  . /usr/bin/check_stack_final_status
fi

delete_stack() {
  check_stack_final_status
  # shellcheck disable=SC2154
  case $stack_status in
    "CREATE_COMPLETE"|"UPDATE_COMPLETE"|"UPDATE_ROLLBACK_COMPLETE"|"ROLLBACK_COMPLETE" \
    |"REVIEW_IN_PROGRESS" \
    |"UPDATE_ROLLBACK_FAILED"|"CREATE_FAILED"|"ROLLBACK_FAILED"|"DELETE_FAILED")
      aws cloudformation update-termination-protection --no-enable-termination-protection --stack-name "$CFN_STACK_NAME"
      aws cloudformation delete-stack --stack-name "$CFN_STACK_NAME"
      cfn-tail "$CFN_STACK_NAME"
      check_stack_final_status
      ;;
    "DELETE_COMPLETE")
      echo "It looks like that stack is already deleted."
      ;;
    *)
      echo "It looks like that stack was never created in the first place."
      ;;
  esac
}
