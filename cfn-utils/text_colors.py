#! /usr/bin/python3
"""Contains constants mapping to ANSI color codes for text output."""

HEADER = '\033[95m'
OKBLUE = '\033[94m'
OK_GREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
END_COLOR = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'
